#
# csv2txt (Python csv to text converter) v1.0 - 02-02-2009
#
# This script converts a comma seperated csv file to a plain text file containing only the 'words' from the original
# csv file. The converter was written because I needed to extract the words from spreadsheet files for the purpose of 
# indexing the spreadsheet files so they could be searched. 
# Whilest I could generally convert to a csv text file this contained a lot of rubbish which was not at all useful as 
# part of the indexing process - this filter gets rid of the rubbish.

# Copyright (C) 2009 Geoff Eagles <geoff.eagles@gmail.com>
# Licensed under the GNU LGPL v2.1 - http://www.gnu.org/licenses/lgpl-2.1.html
# - or any later version.
#

# the file takes 2 parameters - an input filename and an output filename. The filenames need a full path

from os.path import isfile

class csv2txt:

  # perform the conversion
  def convert(self, inputFile, outputFile):
    """Convert a comma seperated csv file into a text file containing just the words from the csv (no commas or quotes)"""

    # open the inputfile for reading and an output file for writing
    infile = open(inputFile,'r')
    #if an output file of that name already exists it will be overwritten - user gets no warning
    outfile = open(outputFile,'w')

    # note that a string object in python cannot be changed - when you append to a string a new object is created so 
    # this code is slow - google around and I'm sure you could find ways to improve it - I don't think it's worth it 
    # in this context
    while 1:
      inpline = infile.readline()
      if not inpline: break    #break out of the while loop when we reach the end of file
      countout = 0
      outstr = ""
      myarray = inpline.split(",")
      nocols = len(myarray)
      #check there's content in the row rather than just blank columns
      if len(inpline) < (nocols + 3):
        continue
      for i in range(nocols):
        if len(myarray[i]) > 1:
          #we need to strip any speech marks
          my1 = myarray[i].replace('"','')
          outstr += my1 + " "
          countout += 1  #damn - python doesn't understand ++
      if len(outstr) > 1:
        #write it to the output file - the rstrip is as much to get rid of stray line feeds as the trailing space
        outfile.write(outstr.rstrip() + '\n')
    #we don't really need to do this in Python but I was brought up to be tidy
    infile.close()
    outfile.close()

# this is where we come in
if __name__ == "__main__":
  from sys import argv, exit
  noargs = len(argv)
  if  noargs != 3:
    if noargs < 3:
      #argv[0] is the name of our converter csv2txt
      print "USAGE: python %s <input-file> <output-file> - Insufficient arguments" % argv[0]
      #can we direct output to stderr??
      exit(255)
    if noargs > 3:
      #argv[0] is the name of our converter csv2txt
      print "USAGE: python %s <input-file> <output-file> - Too many arguments!" % argv[0]
      #can we direct output to stderr??
      exit(255)

  if not isfile(argv[1]):
    print "No such input file: %s" % argv[1]
    exit(1)

  try:
    converter = csv2txt()
    converter.convert(argv[1], argv[2])
  except IOError, (errno, strerror):
    #obviously having trouble opening the input or output file - could be file permissions? should we investigate? 
    if errno == 13:
      print "ERROR: You do not have permission to write an output file"
      exit(1)
    print "ERROR: IO Exception (%s) - %s" % (errno, strerror)
    exit(1)
  except:
    #just dunno what happened - don't think we're left with an output file though
    print "ERROR: Failed to convert file"
    exit(1)
