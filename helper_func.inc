<?php

/* 
 * This file contains all the functions needed to convert documents to text.
 * feel free to add your own conversion functions
 * In the variables table is an array. For each file extension there are one or more entries in the array. Each entry contains
 * the following:
 * > The file extension to be converted. e.g doc, xls etc
 * > The function to achieve the conversion. The function is generally (though not necessarily) used to call an external command-line utility to do the 
 *     actual conversion 
 *
 * Functions are always passed two parameters- the path to the file to convert and the path to the required converted text file
 * Functions always return true or false. False indicates a failure to process the document, true indicates the document has been processed for 
 * indexing. If the function has returned false the calling process may try using another function.
 *
 *
Lines like
  $orig_file = getTpath() . "/" . $frec->fname;
might be improved with realpath to make sure the slashes point the right way on a windows machine
 *
 */



//this function returns the full path to the drupal tmp directory with a trailing slash. ONLY tested for UNIX
function getTpath() {

  //do a test here on the OS and decide which slash to use
  if (substr(PHP_OS, 0, 3) == 'WIN') {
     $slash = "\\";   //backslashes need escaping of course
  }
  else {
    $slash = "/";
  }

  //we don't know which of these paths may be terminated or start with a slash - different sites may be set up differently. Our best bet is
  //to strip and re-add slashes

  //is there a slash on the end of the document root - if not we'll add one
  $len = $_SERVER['DOCUMENT_ROOT'];
  if (strripos($_SERVER['DOCUMENT_ROOT'], $slash) == $len) {
    $tmpRoot = $_SERVER['DOCUMENT_ROOT'];
  }
  else {
     $tmpRoot = $_SERVER['DOCUMENT_ROOT'] . $slash;
  }
  //is there a slash on the start or end of the base path - if so strip the leading slash. Sometimes there is no base path
  //we need to check
  $base = base_path();
  $len = strlen( $base);
  $nobase = FALSE;
  if ($len < 2) {
    $nobase = TRUE;
  }
  else {
    if (stripos($base,$slash) == 1) {
      $tmpBase = substr($base,1);
    }
    else {
      $tmpBase = $base;
    }
    $len = strlen($tmpBase);
    if (strripos($tmpBase, $slash) != $len) {
      $tmpBase .= $slash;
    }
  }

  //check there's actually a valid temp directory (I don't like 1 letter directory names!!)
//  $varrecs = db_query("SELECT * FROM {variable} WHERE name = '%s'", "file_directory_temp");
//  $data = db_fetch_object($varrecs);

    //I DON'T understand - I can't rely on variable get working on all servers???
//  $len = strlen($data->value);
//  $pos = strrpos($data->value,':');  //the minus 2 means we can step over the :" to the start of the value
//  $start = $pos + 2;
//  $wordl = ($len - $start) - 2;
//  $tmp = substr($data->value, $start,  $wordl);

  $tmp = variable_get("file_directory_temp",NULL);

  //strip any leading slash
  if (stripos($tmp, $slash) == 1) {
    $tmp = substr($tmp,1);
  }
  $len = strlen($tmp);
  //add a trailing slash
  if (strripos($tmp,$slash) != $len) {
    $tmp .= $slash;
  }

  if ($nobase) {
    $tmp_path =  $tmpRoot . $tmp;
  }
  else {
    $tmp_path =  $tmpRoot . $tmpBase . $tmp;
  }
  return $tmp_path;
}


/* function to launch the open office application in 'headless' mode - running as a server. It will remain active until we kill it with the function dbfm_open_close */
function dbfm_open_open() {
  //open office command line will have to be set in the variables table because it's different for every server type
  $oopath = variable_get("dbfmsearch_oopath",'/usr/bin/oopath');

  $shell_cmd = '/usr/bin/ooffice "-accept=socket,host=localhost,port=8100;urp;StarOffice.ServiceManager" -norestore -nofirststartwizard -nologo -headless &';

  /*the command will be different for Windows, UNIX and MAC
  WINDOWS
  'soffice.exe "-accept=socket,host=localhost,port=8100;urp;StarOffice.ServiceManager" -norestore -nofirststartwizard -nologo -headless &';
  UNIX
  MAC
  I dunno - I haven't a server to try it on
  */

  //to test office is running successfully use 
  //ps aux | grep soffice
  //what does office run as ? root??

  //you don't need to use the shell exec, PHP will execute if you surround the command string in backticks `` (NOT single quotes)
  $ret_str = shell_exec($shell_cmd);
  //the returned value will generally be a process number. e.g. "[1] 4506", but may be an error condition. It might be worth 
  //saving the process number. It might be valid to detect an error condition based purely on the length of string returned
  //if an error condition is returned we need to set a variable to say NOT to use open office functions
  //shame we can't tell the difference here between stdout and stderr

  //do we want a flag on the file record if we can't perform a conversion? Seems a good idea then we get a new converter (or fix a problem with an existing one) then we can do a query / convert / re-index.

  //test whether it returned OK and set a flag if it didn't

  //wait around for a few *seconds* to be sure it's safe to use Open Office
  sleep(4);
  return true;
}

/* closes the open office application on the server */
function dbfm_open_close() {

  //for unix at least we'll use ps to find what processes are running and then kill them off
  //ZZZZZZZZZZZZZZZZZZ
  return true;
}



//open office conversion from .doc
function ooFromDoc($orig_file, $fileout) {

watchdog2('dbfmsearch', "made the ooFromDoc function for " . $orig_file . " just returns false for the moment", WATCHDOG_NOTICE);

  //open the file's db record

  return false;
}

//catdoc, conversion from .doc. The conversion code is written in C and is relatively swift
function catFromDoc($orig_file, $fileout) {

watchdog2('dbfmsearch', "made the catFromDoc function for " . $orig_file, WATCHDOG_NOTICE);
  //we're assuming in this case that the catdoc utility is on your path
  $script = variable_get("dbfmsearch_catdoc_path",'')."catdoc '" . $orig_file . "' > '" . $fileout . "'";
  $ret_str = shell_exec($script);

  //monitor whether the conversion is complete
  if (finishedYet($fileout, 250000, 1000)) {
    return true;
  }
  else {
    return false;
  }
  return true;
}

/* uses a bash shell script - html2txt - that lives in the module directory */
function txtFromHTML($orig_file, $fileout) {

watchdog2('dbfmsearch', "made the txtFromHTML function for " . $orig_file, WATCHDOG_NOTICE);

  //the bash script we need to run lives in the module directory - we need to prepend the path
  $path = $_SERVER['DOCUMENT_ROOT'] . "/" .  substr(base_path(),1) . drupal_get_path('module', 'dbfmsearch');
  //we don't need to give this script an output file
  $script = $path . "/html2txt '" . $orig_file . "'";
  $ret_str = shell_exec($script);
watchdog2('dbfmsearch', "calling script - " . $script, WATCHDOG_NOTICE);

  //monitor whether the conversion is complete - 
  if (finishedYet($fileout, 250000, 500)) {
    return true;
  }
  else {
    return false;
  }
  return true;  //shouldn't ever reach here
}

//allows a text document to be indexed - this is a dummy function it does nothing except return true - the original file and fileout will be the same
function txt2txt($orig_file, $fileout) {

watchdog2('dbfmsearch', "made the txt2txt function for " . $orig_file, WATCHDOG_NOTICE);

  return true;
}

/* uses a bash shell script to convert from pdf (portable document format) OR from ps (postscript) to text  */
function txtFromPdf($orig_file, $fileout) {

watchdog2('dbfmsearch', "made the txtFromPdf function for " . $orig_file, WATCHDOG_NOTICE);

  //the bash script we need to run lives in the module directory - we need to prepend the path
//  $path = $_SERVER['DOCUMENT_ROOT'] .  "/" . substr(base_path(),1) . drupal_get_path('module', 'dbfmsearch');
//  $script = $path . "/pdf2txt " . $orig_file . " " . $fileout;

  // assumes that ps2ascii is on the search path
  $script = "ps2ascii '" . $orig_file . "' > '" . $fileout . "'";
  $ret_str = shell_exec($script);
  //NOW we have a problem - we have to wait until the conversion is complete before returning - this could take some while with a large file
  //even a 2 meg file may take over a minute to convert so to ensure everything's finished we periodically check on the filesize. If 2 checks report the 
  //same filesize then we assume we're finished.
  //monitor whether the conversion is complete - check every half a second, we'll run for a max of  20 mins
  if (finishedYet($fileout, 500000, 2400)) {
    return true;
  }
  else {
    return false;
  }
  return true;
}

//antiword, conversion from .doc - nah

//uses one of the catdoc applications xls2csv, then further processes the csv file removing the excess commas and empty lines
function catFromXls($orig_file, $fileout) {
  $currentdate = date("Y-m-d H:i:s");

watchdog2('dbfmsearch', "made the catFromXls function for " . $orig_file, WATCHDOG_NOTICE);
  //this is done in two stages since the csv, although plain text, is not really what we're after
  //we need to further filter the csv to end up with just the words and no extra commas and new lines

  //first we have to put a new extension on the filename passed

  //work out the intermediate output filename - strip off the existing extension and append a .csv extension
  $pos = strrpos($orig_file, '.');
  $intout = substr($orig_file, 0, $pos) . ".csv";

  //run the external application xls2csv to convert to a csv file
  $path = $_SERVER['DOCUMENT_ROOT'] .  "/" . substr(base_path(),1) . drupal_get_path('module', 'dbfmsearch');
  $script = $path . "/xls2csv ";

  //build the command to execute the external shell script 
  $shell_cmd = $script . "'" .  $orig_file . "' > '" .  $intout . "'";

  //run the external script  to convert to a text 'intermediate' file with loadsa commas
  $ret_str = shell_exec($shell_cmd);
  //monitor whether the conversion is complete - allow about 3 mins
  if (!finishedYet($intout, 250000, 1000)) {
    //get rid of the intermediate file
    unlink($intout);
    return false;
  }

  //now run a function which converts from csv to plain text
  if (csv2text($intout, $fileout)) {
    //remove the intermediate file before returning
    unlink($intout);
    return true;
  }
  unlink($intout);
  return false;
}

/* converts a csv file into a text file using a pyton script that I wrote */
function csv2text($orig_file, $fileout) {

watchdog2('dbfmsearch', "made the csv2text function for " . $orig_file, WATCHDOG_NOTICE);

  //the python script we need to run lives in the module directory - we need to prepend the path
  $path = $_SERVER['DOCUMENT_ROOT'] .  "/" . substr(base_path(),1) . drupal_get_path('module', 'dbfmsearch');
  $pythonscript = $path . "/csv2txt.py ";

  //build the command to execute the external shell script - what do we do about the path to the python converter??
  //where are we when we run this??
  $shell_cmd = "python " . $pythonscript . "'" . $orig_file . "' '" . $fileout . "'";

  //run the external python script 'csv2txt' to convert to a text file without loadsa commas
  $ret_str = shell_exec($shell_cmd);

  //monitor whether the conversion is complete
  if (finishedYet($fileout, 10000, 100)) {
    return true;
  }
  else {
    return false;
  }
  return true;
}

//default helper function, merely indexes the metadata - it's passed a file record
function def_func($frec) {
  $currentdate = date("Y-m-d H:i:s");

  //the default function where a helper function hasn't been found to deal with the files extension

  //use a function to format the metadata
  $text = formatMeta($frec);

  //we need to escape our text before shoving it in the database
  $etext = mysql_real_escape_string($text);

  //we need to save our 'processed html' in the file record and update the index date - WHAT about index errors (what did I mean?)
//  $queryu = sprintf("UPDATE {dbfm_file} SET ftsearch = %s, flastidx = %s WHERE fid = %d", $etext, $currentdate, $frec->fid);
  $queryu = "UPDATE {dbfm_file} SET ftsearch = '" . $etext . "', flastidx = '" . $currentdate . "' WHERE fid = " . $frec->fid;
  $res = db_query($queryu);
  if ($res == FALSE) {
    //make a note about this in the watchdog
    watchdog2('dbfmsearch', "ERROR (def_func): Unable to update the data file for " . $frec->fid, WATCHDOG_ERROR);
    //this is not a conversion failure - there would be no point in trying again 
  }
  return true;
}

/* this function converts a MS PowerPoint file into a text file */
function catFromPpt($orig_file, $fileout) {
watchdog2('dbfmsearch', "made the catFromPpt function" . $fid, WATCHDOG_NOTICE);

  //build the command to execute the external conversion program - it is assumed that catppt will be on the search path
  $script = "catppt '" . $orig_file . "' > '" . $fileout . "'";
watchdog2('dbfmsearch', "catFromPpt calls " . $script, WATCHDOG_NOTICE);

  //run the external python script 'catppt' to convert to a text file
  $ret_str = shell_exec($script);

  //monitor whether the conversion is complete
  if (finishedYet($fileout, 100000, 1000)) {
    return true;
  }
  else {
    return false;
  }
  return true;
}

//open the dbfm_data record - takes as parameters the file id of the data record to open and the filename to use
//when saving the uncompressed blob. Returns true if everything's hunky dory, otherwise false
function openBlob($fid, $fname) {

  $query = "SELECT fblob FROM {dbfm_data} WHERE fid = " . $fid;
  $datares = db_query($query);
  if ($datares == FALSE) {
    #not able to query the dbfm_data table - this shouldn't happen so make a watchdog entry
    watchdog('dbfmsearch', "ERROR: Unable to query the dbfm_data table: " . $query, WATCHDOG_ERROR);
    return false;
  }

  //uncompress the record - but ONLY if the record is compressed. Some files are excluded from compression.
  //we probably won't ever NOT uncompress - it's only things like zip files etc. that we don't compress in the first place
  //we should be catching those filetypes earlier and calling the metadata-only converter since the files are unlikely to
  //contain text
  $frec = db_fetch_object($datares);
  if ($frec) {
    if (dbfm_compress($extension)) {
      $tempcontents = gzuncompress($frec->fblob);
    }
    else {
      $tempcontents = $frec->fblob;
    }
  }
  else {
    //no data record found 
    watchdog('dbfmsearch', "ERROR: No data record found for fid: " . $fid,WATCHDOG_WARNING);
    return false;
  }
  //now write the uncompressed blob into a file
  $fp = fopen($fname, 'w');
  if ($fp == FALSE) {
    //couldn't write to the file
    watchdog('dbfmsearch', "ERROR (openBlob): Unable to open file for fid: " . $fid, WATCHDOG_WARNING);
    watchdog('dbfmsearch', "ERROR (openBlob): Unable to open filename: " . $fname, WATCHDOG_WARNING);
    return false;
  }
  if (fwrite($fp,$tempcontents) == FALSE) {
    //couldn't write to the file
    watchdog('dbfmsearch', "ERROR: Unable to write file for fid: " . $fid, WATCHDOG_WARNING);
    return false;
  }
  fclose($fp);
  return true;
}

/* 
 * finishedYet? - check whether the file conversion is complete by monitoring the output of that conversion
 *
 * @param $fileout
 *   The full path to the output file from the conversion
 * @param $checkinterval
 *   the number of microseconds between checks. Typically 10000 (one hundredth of a second) would be reasonable - although, if the conversion was 
 *   likely to take a considerable time (e.g a conversion from pdf) you might want to increase this value
 * @param $timeout
 *   The multiple of $checkinterval before we give up - so if $checkinterval is 10000 and $timeout is 100 then we time out after 1 second
 * @return
 *   True if the file seems to have turned up OK. False otherwise.
 */
function finishedYet($fileout, $checkinterval, $timeout) {
  $outsize = 0;
  $notThere = true;
  $ntcount = 0;  //not there count - if no doc after $timeout then we give up
  while(1) {
    usleep($checkinterval);   //wait for a while before checking on the file
    $oldsize = $outsize;
    if ($notThere) {
      if (file_exists($fileout)) {
        $notThere = false;
      }
      else {
   $ntcount = $ntcount + 1;
        if ($ntcount > $timeout) {
          watchdog('dbfmsearch', "Exceded timeout while converting to " . $fileout, WATCHDOG_WARNING);
          return false;
        }
      }
    }
    else {
      $outsize = filesize($fileout);
      if ($outsize == $oldsize) {
        //get out if the file isn't getting any bigger
        break;
      }
    }
  } //end of while loop
  if ($outsize < 30) {
    //doesn't seem to have worked - file has 30 characters or less
    watchdog('dbfmsearch', "Invalid output file - " . $fileout, WATCHDOG_WARNING);
    return false;
  }
  return true;
}

//takes a pointer to a file record as its parameter. Extracts data from the file record and formats it returning it as a string
function formatMeta($frec) {
  //this function uses metadata from the file record to populate a string
  //while the title and filename should have been filled in, description and keywords may be blank. If blank don't include them
  $text = '<p><h1>' . check_plain($frec->ftitle) . ' ' . check_plain($frec->fname) . ' </h1>';
  if (!empty($frec->keywords)) {
     $text .= '<h1> ' . check_plain($frec->keywords) . '</h1>';
  }
  if (!empty($frec->fdesc)) {
    $text .= '<h1>' . check_plain($frec->fdesc) . ' </h1>';
  }
  $text .= '</p>';
  return($text);
}

function watchdog2($ignoreme, $message, $notinterested) {
  $tp = getTpath();
  if ($tp == FALSE) {
    return FALSE;
  }
  $logname = $tp . "mywatchdog.txt";
  //if we can't find an output file then create one, otherwise append to the pre-existing file
  if (file_exists($logname)) {
    //append the message to the file
    $fp = fopen($logname,"a");
  }
  else {
    //open a new logfile
    $fp = fopen($logname,"w");
  }
  //write, close and exit
  fwrite($fp, $message . "\n");
  fclose($fp);
}
