<?php

/**
 * Implementation of hook_install().
 *
 * the install will need to add the extra database fields
 */
function dbfmsearch_install() {

  $currentdate = date("Y-m-d H:i:s");

  variable_set('dbfmsearch_last_index', $currentdate);   //date associated with last file processed

  if (variable_get('replication_server_mode','0') != 2) {
    variable_set('dbfmsearch_last_change', $currentdate);   //date associated with last file processed
    db_query("ALTER TABLE {dbfm_file} ADD `ftsearch` LONGTEXT NULL, ADD `flastidx` DATETIME NULL") ;
  }
  drupal_set_message("dbfmsearch successfully installed");
}



/**
 * Implementation of hook_uninstall().
 * Removes the variables used by dbfmsearch
 *
 * Note that  drupal's indexing will have filled the 3 search tables with indexing information
 * for dbfm documents and I need a way of getting rid of that. The hardest job is weeding out the entries in the search_total table
 * the solution as implemented is NOT perfect and I would recommend re-indexing the site
 *
 * search dataset - is easy, look for entries where the type is "dbfm" - do we need to keep a note of the sid's??
 * search index - again we can delete on the sid/type combination
 * search total - arghh, it has two fields, word and count. The problem is that a word has only a single entry in the table, regardless of how many
 * documents contain the word. For each word you'd need to check through the search index table (by now empty of dbfm stuff) and see whether there are still
 * any entries for the word. If  not then get rid of the entry. If there are then keep the entry. HOWEVER, the count will probably be distorted so it's best to reindex
 * the whole exercise is going to take forever!
 *
 * the uninstall will need to remove the extra database fields
 */
function dbfmsearch_uninstall() {
  //this may take a considerable time to run as it has to tidy up the search tables removing all the dbfm entries then optimising the tables
  //remove the php time limit
  set_time_limit(0);  //switches off the default php timeout

  $ret = array();

  //these commands have to be done on the individual tables - the variable and search tables aren't replicated

  //get rid of all the variables we've used (all variables must start with dbfmsearch_ 
  db_query("DELETE FROM {variable} WHERE name LIKE 'dbfmsearch!_%' escape '!'");
  cache_clear_all('variables', 'cache');

  //clear all the entries we've made in the search tables
  db_query("DELETE FROM {search_dataset} WHERE type = 'dbfm'");
  db_query("DELETE FROM {search_index} WHERE type = 'dbfm'");

  //loop through the entire search_total table if a word in the table can't be found in the search_index then delete the entry
  //lets hope the servers got plenty of memory!
  $srch_query = db_query("SELECT * FROM {search_total}");
  while ($search_rec = db_fetch_object($srch_query)) {
    //hunt through the search_index table trying to find a matching word
    $sq2 = db_query("SELECT word FROM {search_index} WHERE word = '%s'", $search_rec->word);
    $numret = db_num_rows($sq2);
    if ($numret == 0) {
      db_query("DELETE FROM {search_total} WHERE word = '" .  $search_rec->word . "'");
    }
  }
  db_query("ALTER TABLE {dbfm_file}  DROP `flastidx`");
  db_query("ALTER TABLE {dbfm_file}  DROP `ftsearch`");

  //the search tables will now have loads of deleted entries which will slow things down - we need to optimise the tables
  db_query("OPTIMISE TABLE {search_dataset}");
  db_query("OPTIMISE TABLE {search_index}");
  db_query("OPTIMISE TABLE {search_total}");
  //there is absolutely no need to optimise dbfm_file

  drupal_set_message("dbfm_search successfully uninstalled");
}
