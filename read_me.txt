README for dbfmsearch - please read the about.txt file first

Installing the dbfmsearch module

dbfmsearch uses a whole clutch of external applications to perform a conversion on the documents stored in dbFM - converting them to text.

Each external application needs installing so here are a few notes on each of them.

Your server should be running lynx and ghostscript (to do the postscript / pdf conversions).
You need python running on your server
You need a c compiler on the server to compile and install catdoc

there are three files that need to be moved into the top-level drupal directory. These are your cron scripts plus a kill function:
indx_cron.php
conv_cron.php
and kill_cron.php

There are a number of scripts included in the release which should stay in the dbfmsearch directory
csv2txt.py
pdf2txt
html2txt
xls2csv

You need to install catdoc - see notes below.
Optionally you could install openoffice - see notes below.



I had planned to use openoffice to do some of the document conversions, however it's quite a heavyweight application, not especially fast to load or run. Although I've had it working I'm not actually using it at my company at the moment. Nothing to stop you using it (or any other converters you fancy) so I've left the hooks in.
OpenOffice installation - couldn't be simpler, just go to the Open Office download pages:
download.openoffice.org and download whichever version takes your fancy. I'm currently using v2.0.4 (the version Debian consider stable)
I briefly tried version 3 because it seemed to offer the possibility of dealing with pdf files. But no! it doesn't really cut it as regards pdf's yet so I wouldn't bother if I were you.
If you don't have a GUI on your server it makes life a little more interesting. However there are plenty of instructions out there if you just google.
It is posible to shave a chunk off the load time by not loading the java runtime.
I don't know how you switch it off from the command line (if anyone does know please get in touch)
What you can do is launch the full application with its GUI. Go to Tools/Options/OpenOffice.org/Java and deselect "Use a Java Runtime"

Configuring open office to work with dbfm:
you need to set the appropriate paths on the dbfmsearch admin page
the default conversions have been pre-entered, if you don't propose to use open-office to do your conversions then you can remove these entries - of course if you have a document with an open source extension then what're you going to do?

catdoc comes from http://vitus.wagner.pp.ru/software/catdoc
There are 3 programs here:
catdoc, catppt and xls2csv
The catdoc package needs to be installed (for which you'll need a c compiler on your machine).

xls2csv you use in the following way:
xls2csv your.xls > your.csv
The program seems to do a far better job than the conversion performed by Open Office.

Another possibility
antiexcel - this is a small shell script which fires off another script written in perl which turns the xls file into a csv file


The basic way I handle each converter is that I keep testing the size of the output file to detect when the script / function is finished. Just checking whether an output file exists is no good. The file will, almost certainly exist BUT may still be mid-conversion. So we need to check the output filesize and establish when it stops growing. We mustn't check too often because the files may grow in fits and starts. So we should check if the filesize is the same two tests in a row, if so then assume the conversion is complete. The functions that call each converter can be adjusted to call at different intervals.