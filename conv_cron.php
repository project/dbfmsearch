<?php

/**
 * @file
 * sconv search conversions. Calls functions to convert documents from their native formats into plain text for search indexing purposes
 */

include_once './includes/bootstrap.inc';

drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
//having loaded all the drupal bits we need we can get to business!

$ret =  dbfmsearch_conv();


