<?php

/**
 * @file
 * sconv search conversions. Functions to convert documents from their native formats into plain text for search indexing purposes
 */

include_once './includes/bootstrap.inc';

drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
//having loaded all the drupal bits we need we can get to business!

//the include file contains all the functions used when converting documents - do I need this??
// $mpath =  drupal_get_path('module', 'dbfmsearch');
//include_once $mpath .  '/helper_func.inc';
$ret =  dbfmsearch_conv();

//thats all - although we may also need to move indexing in here as well


